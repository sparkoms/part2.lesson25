<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Students</title>
    <meta charset="UTF-8"/>
</head>
<body>
<h1>Students</h1>
<ul>
    <li><a href="${pageContext.request.contextPath}/person/list">List students</a></li>
    <li><a href="${pageContext.request.contextPath}/person">New student</a></li>
    <li><a href="${pageContext.request.contextPath}/suser/list">List users</a></li>
    <li><a href="${pageContext.request.contextPath}/user">New user</a></li>
</ul><br>
<a href="${pageContext.request.contextPath}/logout">Logout</a>
</body>
</html>

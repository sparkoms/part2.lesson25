<%@ page import="ru.inno.stc14.entity.User" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: DNS
  Date: 12.06.2019
  Time: 21:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Users</title>
    <meta charset="UTF-8" />
</head>
<body>
<table border="1">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Password</th>
    </tr>
    <% List<User> list = (List<User>) request.getAttribute("userList");
        for (User user : list) { %>
    <tr>
        <td><%=user.getId()%></td>
        <td><%=user.getName()%></td>
        <td><%=user.getPassword()%></td>
    </tr>
    <br>
    <% } %>
</table>
<br>
<a href="${pageContext.request.contextPath}">Main page</a><br>
<a href="${pageContext.request.contextPath}/logout">Logout</a>
</body>
</html>
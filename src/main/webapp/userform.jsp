<%--
  Created by IntelliJ IDEA.
  User: DNS
  Date: 14.06.2019
  Time: 22:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>New user</title>
</head>
<body>
<h2>Adding a new user</h2>
<form method="post" action="${pageContext.request.contextPath}/user">
    <input type="text" name="name" placeholder="name"><br/>
    <input type="password" name="password" placeholder="password"><br/>
    <input type="submit"/>
</form>
<a href="${pageContext.request.contextPath}">Main page</a><br>
<a href="${pageContext.request.contextPath}/logout">Logout</a>
</body>
</html>

package ru.inno.stc14.dao.jdbc;

import ru.inno.stc14.dao.ConnectionManager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Класс, создающий подключение к БД PostgreSQL
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class PGDBConnectionManager implements ConnectionManager {

    private Connection connection;

    public PGDBConnectionManager(String dbURL, String user, String pwd) throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        this.connection = DriverManager.getConnection(dbURL, user, pwd);
    }

    @Override
    public Connection getConnection() {
        return this.connection;
    }
}

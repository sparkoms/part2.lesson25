package ru.inno.stc14.dao.jdbc;

import ru.inno.stc14.dao.ConnectionManager;

import java.sql.SQLException;

/**
 * Класс, являющийся фабрикой для двух типов подключений (через H2 и PostreSQL)
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class ConnectionFactory {

    /**
     * Метод загрузки класса
     * @param typeDB - тип базы данных
     * @param dbURL - строка подключения к БД
     * @param user - имя пользователя БД
     * @param pwd - пароль пользователя БД
     * @return ConnectionManager для определенного типа БД
     */
    public ConnectionManager getConnectionManager(String typeDB, String dbURL, String user, String pwd) throws SQLException, ClassNotFoundException {
        typeDB = typeDB.toLowerCase();

        if (typeDB.equals("h2")) {
            return new H2DBConnectionManager(dbURL, user, pwd);
        } else if (typeDB.equals("pg")) {
            return new PGDBConnectionManager(dbURL, user, pwd);
        } else {
            throw new SQLException("Неверно указан тип базы данных");
        }
    }
}
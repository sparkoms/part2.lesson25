package ru.inno.stc14.dao.jdbc;

import ru.inno.stc14.dao.ConnectionManager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Класс, создающий подключение к БД H2
 * @autor Виктор Леонтьев
 * @version 1.0
 */
public class H2DBConnectionManager implements ConnectionManager {
    private Connection connection;

    public H2DBConnectionManager(String dbURL, String user, String pwd) throws ClassNotFoundException, SQLException {
        Class.forName("org.h2.Driver");
        this.connection = DriverManager.getConnection(dbURL, user, pwd);
    }

    public Connection getConnection() {
        return this.connection;
    }
}
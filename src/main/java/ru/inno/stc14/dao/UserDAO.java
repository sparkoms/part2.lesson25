package ru.inno.stc14.dao;

import ru.inno.stc14.entity.User;
import java.util.List;

public interface UserDAO {

    List<User> getList();

    boolean addUser(User user);

    boolean isExistUser(User user);
}
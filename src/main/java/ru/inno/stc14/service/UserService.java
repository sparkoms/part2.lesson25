package ru.inno.stc14.service;

import ru.inno.stc14.entity.User;
import java.util.List;

public interface UserService {
    List<User> getList();
    boolean addUser(String name, String password);
    boolean isExistUser(String name, String password);
}
